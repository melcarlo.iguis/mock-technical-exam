function countLetter(letter, sentence) {
    let result = 0;

    if(letter.length == 1){
        for(i=0; i<sentence.length; i++){
            if(sentence[i] === letter){
                result += 1;
            }
        }
        return(result)
    }else{
        return(undefined)
    }

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
    
    
}


function isIsogram(text) {

    for (let i = 0; i < text.length; i++) {
        for (let j = 0; j < text.length; j++) {
            if(i!=j){
              if (text[i] == text[j]) {
                  return false
              }
          }
        }
    }
    return true;
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    
}

function purchase(age, price) {


    if(age< 13){
        return(undefined)
    }else if(age >=13 && age <=21 || age >=65){
        let discount = price * 0.8
        const roundedPrice = discount.toFixed(2);
        return(roundedPrice.toString())
    }else if(age >= 22 && age <=64){
        return(price.toFixed(2).toString())
    }

    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {

    const noStockitems = items.filter(noStock);

    function noStock(items){
        return items.stocks == 0;
    }

    let newArr = []

    for(i=0; i<noStockitems.length; i++){
        let item = noStockitems[i].category 
        newArr.push(item)
    }

    // for loop for checking duplicates
    for (var i = 0; i < newArr.length; i++) {
        for (var j = 0; j < newArr.length; j++) {
            if(i!=j){
                if (newArr[i] == newArr[j]) {
                    newArr.splice(i,1)
                    return(newArr)
                }
            }
        }
    }

    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {


    const flyingVoter = []
    
    for (var i = 0; i < candidateA.length; i++) {
        for (var j = 0; j < candidateB.length; j++) {
            // if the index of two array are not equal then compare the data
            if(i!=j){
                if (candidateA[i] == candidateB[j]) {
                    flyingVoter.push(candidateA[i])
                }
            }
        }
    }

    return(flyingVoter)
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};